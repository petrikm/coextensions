coextensions
============

This program serves to generate all one-element coextensions of a given finite,
negative totally ordered monoid (abbreviated by f.n. tomonoid).

* The program is supposed to be interpreted by Python 2.
* Run this program by typing 

    python coextensions.py

* See [EXAMPLES.md](EXAMPLES.md) for some hints how to run the program.


Important note
--------------

The development of this project has stopped since its code has been rewritten
to Python 3 as a Python package and released under a new name:
[fntom](https://gitlab.com/petrikm/fntom).

The package [fntom](https://pypi.org/project/fntom/) has been furthermore
uploaded to [PyPI](https://pypi.org/).

Hence, this project is considered as obsolete and it is recommended to work
with [fntom](https://gitlab.com/petrikm/fntom).


Theoretical notions
-------------------

A _f.n. tomonoid_ is a structure _(S; &le;, &ast;, 1)_ such that:
* _S_ is a finite set
* _&le;_ is a total order on _S_ with the top element _1_
* _&ast;: S &times; S &rightarrow; S_ is a binary operation on _S_ such that, for every _x,y,z &isin; S_:
    - _(x &ast; y) &ast; z = x &ast; (y &ast; z)_,
    - _x &ast; 1 = 1 &ast; x = x_,
    - _y &le; z_ implies _x &ast; y &le; x &ast; z_ and _y &ast; x &le; z &ast; x_.

A f.n. tomonoid is _commutative_ if and only if _x &ast; y = y &ast; x_ for every _x,y &isin; S_.

A f.n. tomonoid is _Archimedean_ if and only if for every _x &le; y_ there is a natural
number _n_ such that _y&#8319; &le; x_.
(Here, _y&#8319; = y &ast; y &ast; ... &ast; y_ (_n_ times).)


References
----------

For the theoretical description of the problem and for the description of the algorithm se the paper:

* M. Petrik and Th. Vetterlein. 
    *Rees coextensions of finite, negative tomonoids.*
    Journal of Logic and Computation 27 (2017) 337-356. 
    DOI: [10.1093/logcom/exv047](https://doi.org/10.1093/logcom/exv047),
    [PDF](papers/Petrik_Vetterlein__Coextensions__preprint.pdf).

For a more detailed description of the algorithm see the papers:

* M. Petrik and Th. Vetterlein. 
    *Algorithm to generate finite negative totally ordered monoids.*
    In: IPMU 2016: 16th International Conference on Information Processing 
        and Management of Uncertainty in Knowledge-Based Systems.
    Eindhoven, Netherlands, June 20-24, 2016.
    [PDF](papers/Petrik_Vetterlein__IPMU_2016__preprint.pdf).

* M. Petrik and Th. Vetterlein. 
    *Algorithm to generate the Archimedean, finite, negative tomonoids.*
    In: Joint 7th International Conference on Soft Computing 
        and Intelligent Systems and 15th International Symposium on Advanced Intelligent Systems.
    Kitakyushu, Japan, Dec. 3-6, 2014.
    DOI: [10.1109/SCIS-ISIS.2014.7044822](https://doi.org/10.1109/SCIS-ISIS.2014.7044822).
    [PDF](papers/Petrik_Vetterlein__SCIS_ISIS_2014__preprint.pdf).
    
For more details on one-element coextensions of finite, negative, tomonoids see the paper:

* M. Petrik and Th. Vetterlein. 
    *Rees coextensions of finite tomonoids and free pomonoids.*
    Semigroup Forum 99 (2019) 345-367. 
    DOI: [10.1007/s00233-018-9972-z](https://doi.org/10.1007/s00233-018-9972-z),
    [PDF](papers/Petrik_Vetterlein__Pomonoids__preprint.pdf).

