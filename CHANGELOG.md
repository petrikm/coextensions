
Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.3] - 2021-02-15
### Added
 *  CHANGELOG.md
 *  journal and proceedings articles related to this code:
    - papers/Petrik_Vetterlein__Coextensions__preprint.pdf
    - papers/Petrik_Vetterlein__IPMU_2016__preprint.pdf
    - papers/Petrik_Vetterlein__Pomonoids__preprint.pdf
    - papers/Petrik_Vetterlein__SCIS_ISIS_2014__preprint.pdf

## [1.0.2] - 2021-01-14
 *  The program has been copied to gitlab.com.
### Added
 *  README.md and EXAMPLES.md

## [1.0.1] - 2020-08-30
### Changed
 *  Comments and descriptions have been rewritten throughout the source in
    order to reflect the correct terminology (e.g. "elementary extensions have
    been renamed to "one-element coextensions", etc.).

## [1.0.0] - 2015-02-02
 *  A fully functioning program has been written (with no track of its history).
 *  The program can find all the co-extensions of a given f.n.tomonoid.
 *  The program can find all the f.n.tomonoids up to a given size.
 *  It is possible to reduce the search to Archimedean or commutative f.n.tomonoids only.

[1.0.3]: https://gitlab.com/petrikm/coextensions/-/tags/1.0.3
[1.0.2]: https://gitlab.com/petrikm/coextensions/-/tags/1.0.2
[1.0.1]: https://gitlab.com/petrikm/coextensions/-/tags/1.0.1
[1.0.0]: https://gitlab.com/petrikm/coextensions/-/tags/1.0.0

