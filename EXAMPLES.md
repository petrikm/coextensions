
Examples
========

Here are some examples how to run the program.
Note that the program is suposed to be interpreted by Python 2.

* Generate all the finite, negative tomonoids by the "brute force" method up to
size 5 and store the resulting report to the files:
`output_5_brute.txt`
and
`output_5_brute.p`

```python
    generateAndStore(5, "brute", False, False)
```


* Generate all the Archimedean, finite, negative tomonoids by the "level set
based" method up to size 6 and store the resulting report to the files:
output_6_levelset_A.txt
and
output_6_levelset_A.p

```python
    generateAndStore(6, "levelset", True, False)
```


* Generate all the commutative, finite, negative tomonoids by the "brute force"
method up to size 7 and store the resulting report to the files:
output_7_brute_C.txt
and
output_7_brute_C.p

```python
    generateAndStore(7, "brute", False, True)
```


* Generate all the Archimedean, commutative, finite, negative tomonoids by the
"level set based" method up to size 8 and store the resulting report to the
files:
output_8_levelset_A_C.txt
and
output_8_levelset_A_C.p

```python
    generateAndStore(8, "levelset", True, True)
```


* Generate all the commutative one-element coextensions of a commutative, finite,
negative tomonoid specified by its multiplication table.
The resulting one-element coextensions are then printed to console.

```python
    table= [['0','t','u','v','w','x','y','z','1'],
            ['0','0','u','u','w','x','x','z','z'],
            ['0','0','0','t','u','u','u','x','y'],
            ['0','0','0','0','u','u','u','x','x'],
            ['0','0','0','0','0','u','u','w','w'],
            ['0','0','0','0','0','0','t','u','v'],
            ['0','0','0','0','0','0','0','u','u'],
            ['0','0','0','0','0','0','0','0','t'],
            ['0','0','0','0','0','0','0','0','0']]
    counter = Counter()
    tom = TomonoidFNC(counter.getNew())
    tom.createFromCharTable(table)
    tom.showShort()
    tom.computeExtensions(counter)
    print "(There are", len(tom.extensions), "one-element coextensions of this f.n. tomonoid.)"
    print
    for ext in tom.extensions:
        ext.showShort()
```
